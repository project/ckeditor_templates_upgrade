<?php

namespace Drupal\ckeditor_templates_upgrade\Plugin\QueueWorker;

use DOMWrap\Document;
use Drupal\ckeditor_templates_upgrade\Event\AlterIncludeNestedEvent;
use Drupal\ckeditor_templates_upgrade\UpgradeUtility;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Processes text fields to wrap existing template embeds.
 *
 * @QueueWorker(
 *   id = "ckeditor_templates_upgrade",
 *   title = @Translation("Upgrade ckeditor template embeds."),
 *   cron = {"time" = 5}
 * )
 *
 */
class EntityQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * FieldQueueWorker constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, QueueFactory $queueFactory, EntityTypeManagerInterface $entityTypeManager, EventDispatcherInterface $event_dispatcher, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queueFactory = $queueFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger_factory->get('ckeditor_templates_upgrade');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue'),
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entityTypeId = $data['entity_type'];
    $entityId = $data['id'];
    $replacementPerformed = FALSE;

    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entityStorage->load($entityId);

    if (empty($entity)) {
      return;
    }

    $fieldDefinitions = $entity->getFieldDefinitions();

    foreach ($entity->getTranslationLanguages() as $langcode => $language) {
      $translation = $entity->getTranslation($langcode);

      foreach ($fieldDefinitions as $fieldDefinition) {
        if (!in_array($fieldDefinition->getType(), UpgradeUtility::PROCESSED_FIELD_TYPES)) {
          continue;
        }

        $fieldItemList = $translation->get($fieldDefinition->getName());
        if ($fieldItemList->isEmpty()) {
          continue;
        }

        for ($i = 0; $i < $fieldItemList->count(); $i++) {
          $item = $fieldItemList->get($i);
          $value = $item->getValue();
          $originalValue = $value['value'];
          $wrapped = $this->wrapTemplateEmbeds($value['value'], $data['selectors']);

          if (!empty($wrapped) && $wrapped != $originalValue) {
            $value['value'] = $wrapped;
            $item->setValue($value);
            $fieldItemList->set($i, $item);
            $replacementPerformed = TRUE;
          }
        }
      }
    }

    if ($replacementPerformed) {
      $this->logger->notice('Wrapped ' . $entityTypeId . ' entity ' . $entityId);
      $entity->save();
    }
  }

  /**
   * Wraps existing template embeds with ckeditor templates wrapper elements.
   *
   * @param string $value
   *   Value of a formatted text field like a node body.
   *
   * @return string
   *   Field value with updated template embeds.
   */
  protected function wrapTemplateEmbeds(string $value, array $selectors) : ?string {
    \libxml_use_internal_errors(TRUE);
    $doc = new Document();
    $doc->setLibxmlOptions(\LIBXML_HTML_NOIMPLIED | \LIBXML_HTML_NODEFDTD);
    $doc->html($value);
    $replacement_performed = FALSE;
    $template_wrapper_selector = '.ckeditor-template-content';

    foreach ($selectors as $template_id => $selector) {
      $nodes = $doc->find($selector);
      if (!$nodes->count()) {
        continue;
      }

      foreach ($nodes as $node) {
        // If the immediate parent is a template wrapper, avoid re-wrapping the
        // current DOM node.
        $parent = $node->parent($template_wrapper_selector);
        if (!empty($parent)) {
          continue;
        }

        // Check if the DOM node is contained in another DOM node which was
        // already wrapped.
        $parents = $node->parents($template_wrapper_selector);
        if ($parents->count()) {
          $include_list = drupal_static('ckeditor_templates_upgrade_skiplist');
          if (is_null($include_list)) {
            // @todo move dispatching of this event to the install hook.
            /** @var \Drupal\ckeditor_templates_upgrade\Event\AlterIncludeNestedEvent $event */
            $event = $this->eventDispatcher->dispatch(new AlterIncludeNestedEvent(array_keys($selectors)));
            $include_list = $event->getIncludeNestedTemplates();
          }
          // If a template was removed from the list of templates that are
          // enabled for wrapping inside an already wrapped DOM node, don't wrap.
          if (!in_array($template_id, $include_list)) {
            continue;
          }
          $this->logger->notice('Nested wrapping detected with selector ' . $selector . ' and include list ' . implode(', ', $include_list));
        }

        $this->logger->notice('Wrapping markup that contains ' . $selector);
        // Append the template markup to a wrapper which will be up-casted by the
        // ckeditor5 version of the ckeditor_templates module as a widget inside
        // the editor.
        $node->wrap('<section class="ckeditor-template-wrapper" data-legacy-template="true"><div class="ckeditor-template-content" data-legacy-template="true"></div></section>');
        $replacement_performed = TRUE;
      }
    }
    return $replacement_performed ? $doc->document()->saveHTML() : NULL;
  }

}
